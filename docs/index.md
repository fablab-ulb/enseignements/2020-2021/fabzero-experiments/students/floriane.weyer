#### Qui je suis
* **Prénom** : Floriane
* **Nom** : Weyer
* **Âge** : 30 ans
* **E-mail** : floriane.weyer@gmail.com


![Photo de profil](./images/photo_de_profil.jpg)

#### D'où je viens
Depuis toute petite, j'aime me balader, explorer, observer la nature. J'ai toujours été curieuse et eu envie de comprendre le monde qui m'entoure. Je pense que c'est de là que vient mon intérêt pour les sciences en général et la physique en particulier.

En 2008, j'ai commencé des études de physique à l'Université de Liège. J'ai découvert que c'est la physique de tous les jours qui me passionne, celle qui essaye de comprendre les phénomènes quotidiens : comment refroidir efficacement un café, pourquoi des gouttes de rosée se forment-elles, comment expliquer la forme les flocons de neige, ... À la fin de mes études, j'ai débuté une doctorat au sein du [_GRASP_](https://www.grasp.uliege.be/cms/c_4010245/en/grasp-portail), un groupe de recherche de la matière molle. Le but de ma thèse était de manipuler de petites gouttes à l'aide de fibres : les déplacer, les diviser, les recombiner.

![Goutte zigzag](./images/goutte_zigzag.jpg)

En 2017, une fois mon doctorat en poche, j'ai eu la chance de travailler pendant 6 mois pour une A.S.B.L. de vulgarisation scientifique, [_Science et Culture_](http://www.sci-cult.ulg.ac.be). J'ai essayé de sensibiliser des jeunes à la beauté des sciences, en réalisant devant eux des expériences marquantes.  

![Résumé des commandes](./images/mousse_en_feu.jpg)

J'ai ensuite repris ma vie de chercheuse en partant aux États-Unis, dans l'Utah, pour un post-doctorat de 6 mois. C'était pour moi l'occasion de faire de la recherche et d'expérimenter la vie à l'étranger. Je suis revenue en Belgique en 2019 pour entamer un nouveau post-doctorat en collaboration aves l'Université de Liège et l'Université Libre de Bruxelles. Je travaille à nouveau sur les gouttes mais j'essaye, cette fois, de collecter le brouillard pour récupérer de l'eau potable.

La recherche me plaît parce que chaque jour est différent. Il y a un coté très studieux où il faut apprendre, étudier, se former mais aussi un coté très pragmatique où il faut construire un dispositif expérimental et réaliser les manipulations. D'autre part, j'aime aussi partager mon enthousiasme pour les sciences avec les plus jeunes. Dans le futur, j'aimerais associer ces deux aspects dans un seul et même métier.

#### Où je vais
J'ai souhaité participer à ce cours non seulement pour me former aux machines numériques mais aussi pour apprendre à les utiliser lors de la réalisation de dispositifs expérimentaux. Il s'agit pour moi d'une nouvelle manière d'envisager la recherche en laboratoire. J'espère ainsi compléter ma "boîte à outils" et nourrir ma recherche en m'essayant à de nouveaux exercices et à l'approche interdisciplinaire.
