#define LED 15
#define BUTTON 8 
#define INTERVAL 1000
#define DEBOUNCE 10

void setup() {
  // put your setup code here, to run once:
  pinMode(15,OUTPUT);
  pinMode(8, INPUT_PULLUP);
  digitalWrite(LED, LOW);
  SerialUSB.begin(0);
}

void loop() {
  // put your main code here, to run repeatedly:
  if(!digitalRead(BUTTON)){
    digitalWrite(LED, !digitalRead(LED));
    delay(DEBOUNCE);
    while(!digitalRead(BUTTON)){}
    delay (DEBOUNCE);
  }

}
