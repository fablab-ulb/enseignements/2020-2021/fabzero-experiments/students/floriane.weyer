# Final Project

Pour le projet final, Stéphanie et moi avons décidé de travailler ensemble. Cette idée nous est apparue assez naturellement étant donné que nous sommes en train de mettre en place un projet professionnel commun : le STEAM Lab.

 Le STEAM Lab est la cellule dédiée à la diffusion des sciences et des outils numériques qui se développe au sein du FabLab ULB. Dans un futur proche, il acceuillera des jeunes, enfants et adolescents, pour réaliser des activités scientifiques d'éducation par la recherche en immersion dans un fab lab en effervescence. STEAM est l'acronyme de Science, Technology, Engeneering, Arts and Mathematics.

Notre projet final présenté ici s'intrique à celui de la mise en place du STEAM Lab et de la conception des activités qui y seront proposées.

Le texte de ce site a été rédigé en collaboration avec Stéphanie Krins. La [documentation de Stéphanie](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/final-project/) est complémentaire à celle-ci, certains passages sont identiques.

## La question comme point de départ

**Quelle expérience peut-on proposer à des enfants pour les émerveiller et leur faire acquérir de nouvelles compétences en créant quelque chose?**

Cette question rassemble différents points qui sont très importants à nos yeux :

* l'expérience : nous souhaitons que les participants à nos activités vivent une _expérience_, un moment pendant lequel ils explorent, ils essaient des choses, ils vont au delà de ce qu'ils connaissent, ils font des erreurs et apprennent de celles-ci;
* l'émerveillement : nous souhaitons mettre en place une activité durant laquelle leurs _sens_ et des _émotions positives_ sont sollicités;
* nouvelles compétences : nous souhaitons leur faire découvrir et expérimenter diverses activités telles que les _brainstormings_, la découverte et la prise en main des divers _outils numériques_ présents dans un fablab (imprimante 3D, découpeuse laser, découpeuse vinyle, électronique de base, ...) ;
* créer quelque chose : nous souhaitons donner la possibilité aux participants de créer un objet par eux-mêmes, de devenir un artisan numérique et ainsi de pratiquer _l'apprentissage par le faire_ et d'obtenir  littéralement une _réalisation_ de leurs acquis.

## Réflexion

La question de base montre notre envie, non seulement, de former les enfants à de nouvelles techniques mais aussi de les émerveiller, de piquer leur curiosité. Une fois cette question formulée, il faut passer au concret... Comment emmener avec nous une classe de 20 élèves vers la découverte des outils du FabLab? Une présentation des machines? Non, trop théorique. Une démonstration de ces machines? Mieux, mais les enfants sont encore spectateurs. Leur faire utiliser les machines, alors? Oui, clairement ... mais dans quel but?

L'idée est alors venue de proposer une immersion-fiction. Le principe d'une immersion-fiction est de plonger les participants dans une situation fictive pour stimuler leur imagination et leur créativité. Quelle histoire proposer à des enfants pour les initier aux machines du FabLab? Stéphanie et son imagination débordante ont proposé l'histoire d'un robot-extraterrestre venu sur terre pour collecter des informations sur le futur de notre planète. Ce visiteur aimerait ramener avec lui, pour un congrès intergalactique, une description de la vie sur terre dans un futur plus ou moins proche, 2500. Problème : il ne possède qu'une clé USB! Le seul moyen de l'aider est donc de créer des fichiers numériques à télécharger sur cette clé pour qu'il puisse les emporter avec lui.

Cette immersion laisse libre cours à l'imagination des élèves et leur permet d'inventer et de créer des objets du futur. Ils sont pleinement acteurs de ce projet. Le fait de devoir communiquer des fichiers numériques au robot apporte une contrainte qui justifie pleinement le recours aux programmes de dessin 2D et 3D et aux machines qui utilisent ces fichiers (découpeuse laser ou vinyle et imprimante 3D).

Notez que ce concept d'enseignement en immersion a déjà été testé au FabLab ULB dans un cours donné par Denis Terwagne et Victor Levy à des étudiants en Sciences et en Architecture. Vous trouverez [ici](https://zenodo.org/record/3906207#.YKIo_CY69H4) un article qui en parle. Cette idée avait d'ailleurs inspiré le physicien et vulgarisateur français Julien Bobroff qui a créé le groupe de recherche "La physique autrement". Julien et son équipe avaient mis sur pied une immersion-fiction de trois jours non-stop avec ses étudiants en licence de science (plus d'informations [ici](http://hebergement.universite-paris-saclay.fr/supraconductivite/projet/immersion_scientifique/)).

## Rencontre avec notre mentor

Afin de mettre sur pied notre projet, nous avons rencontré Julie Degroote, institutrice primaire à l'[école communale de l'Envol](https://www.ecolenvol.net/) à Faulx-les-Tombes (Gesves, Namur). Julie est une enseignante passionnée qui pratique la pédagogie active depuis de nombreuses années. Sa classe regroupe 22 enfants de 1e, 2e, 3e et 4eme primaire. La classe de Julie est souvent appelée "la yourte" étant donné le lieu insolite et convivial dans lequel les enfants sont accueillis.

Nous avons exposé à Julie notre projet et nous lui avons parlé des activités scientifiques d'apprentissage par la recherche que nous souhaitons mettre en place en utilisant les outils numériques du fab lab. Julie s'est montrée très enthousiaste et a accepté de nous épauler. Elle a proposé que nous venions dans la yourte durant 6 après-midis (1x/semaine pendant 6 semaines) pour réaliser un **projet pilote** avec ses élèves.

## Le contexte fictif

Quelle histoire pourrait-on bien raconter aux enfants pour faire travailler leur imaginaire et les insciter à créer quelque chose en utilisant les machines numériques du fablab ?

Avec Stéphanie, nous avons imaginé cette histoire :

_En me baladant dans le sous-sol du FabLab ULB, je découvre une valise noire. Dans cette valise, un joli petit robot vert. Ne sachant pas trop bien à quoi cet objet pouvait bien servir, je reprends la valise chez moi... Cette nuit-là, je suis réveillée par un bruit étrange... "Tic, tic, tic", "Tic, tic, tic"... Le bruit vient de la valise, c'est sûr ! Un peu anxieuse, je m'approche de la mallette noire, l'ouvre et entends le robot prononcer un message à destination des êtres humains._

En résumé, le robot est venu sur terre pour collecter des informations sur le futur de notre planète. Il aimerait ramener avec lui, pour un congrès intergalactique, une description de la vie sur terre dans un futur plus ou moins proche, 2500. Problème : il ne possède qu'une clé USB! Le seul moyen de l'aider est donc de créer des fichiers numériques à télécharger sur cette clé pour qu'il puisse les emporter avec lui.

Cette immersion laisse libre cours à l'imagination des élèves et leur permet d'inventer et de créer des objets du futur. Ils sont pleinement acteurs de ce projet. Le fait de devoir communiquer des fichiers numériques au visiteur apporte une contrainte qui justifie pleinement le recours aux programmes de dessin 2D et 3D et aux machines qui utilisent ces fichiers (découpeuse laser ou vinyle et imprimante 3D).

## Projet

L'immersion-fiction ainsi que sa mise en œuvre dans une classe de primaire constitue notre projet final et notre réponse à la question de départ. Ce projet reprend à la fois :

* la conception et la création du robot-extraterrestre, fil rouge de notre activité;
* la mise en place de la fiction au sein de la classe de primaire;
* l'initiation des enfants au dessin 3D et à l'imprimante 3D;
* l'initiation au dessin 2D et aux découpeuses laser et vinyle.


#### Le robot-extraterrestre

Pour rendre notre fiction concrète et réelle pour les enfants, nous avons décidé de créer un visiteur en utilisant l'impression 3D et d'enregistrer des messages pour qu'il interagisse avec la classe.

L'objet doit donc ressembler à un extraterrestre ou à un robot et renfermer une clé (cachée, c'est plus fun).

Pour ajouter un peu de challenge à ce projet, nous avons décidé de nous mettre une contrainte supplémentaire : n'utiliser que [Tinkercad](https://www.tinkercad.com/) pour le design 3D. C'est cet outil que les élèves seront amenés à utiliser, il est donc indispensable que nous puissions nous-même le manier.

###### L'objet de base

Dans la bibliothèque de [Tinkercad](https://www.tinkercad.com/search), nous avons trouvé ce mignon petit [robot](https://www.tinkercad.com/things/jTLbbpAaYXg-cute-robot-what-i-call-ardorabot). Nous avons importé ce fichier et nous l'avons modifié pour qu'il réponde à nos attentes.

![Robot](./images/robot_base.jpg)

###### La clé USB

Nous voulions cacher une clé USB dans ce robot. Nous avons donc séparé la tête du corps et créé un trou de la taille d'une clé USB.

Pour s'assurer des dimensions de cette cache, nous avons réalisé des tests de calibration. Les dimensions de la clé sont les suivantes :4mm x 12mm x 40mm. La hauteur nous importait peu. Par contre, il fallait s'assurer que la base soit suffisamment large pour mettre la clé mais pas trop pour que la clé ne bouge pas latéralement. Nous avons donc réalisé un banc de test pour clé USB avec des trous de différentes dimensions. Nous avons fait des encoches de 4mm x 12mm, 4.5mm x 12.5mm, 5mm x 13mm, 5.5mm x 13.5mm (schéma à venir).

[Fichier .stl pour calibrer le trou pour la clé USB](../files/gabarit_USB.stl)

**Résultat : 5mm x 13mm.**

![USB](./images/portusb.jpg)


###### Le système de fixation

Comme nous avons décapité le robot pour y mettre une clé USB, nous avons dû imaginer un système de fixation pour que la tête tienne au corps.

Nous avons décidé d'utiliser deux aimants pour maintenir ensemble le corps et la tête. Nous avons créé deux encoches à la dimension des aimants. Encore une fois, nous avons réalisé un banc de test pour trouver les dimensions idéales afin que l'aimant rentre parfaitement dans le trou sans en ressortir par la suite. Les aimants sont des cylindres de 8.01mm de diamètre et de 3.02mm de haut. Nous avons fait des trous de 7.61, 7.81, 8.01, 8.21 et 8.41mm de diamètre et de 2.62, 2.82, 3.02, 3.22 et 3.42 mm de haut, respectivement.

[Fichier .stl pour calibrer le trou pour l'aimant](../files/gabarit_aimant.stl)

**Résultat : 8.01mm de diamètre et 3.02 de haut.**

![Aimant](./images/aimant.jpg)

Pour s'assurer de l'alignement entre les deux parties, nous avons choisi d'utiliser la méthode de [couplage cinématique de Maxwell](https://en.wikipedia.org/wiki/Kinematic_coupling) qui utilise 3 demi-sphères d'un coté et 3 creux en forme de V de l'autre coté. Lorsque les sphères entrent en contact avec les faces inclinées des encoches, il n'existe plus de degré de liberté et les deux parties s'alignent. Nous avons donc ajouté 3 sphères dans le bas de la tête et 3 encoches dans le haut du corps.

[Fichier .stl pour calibrer le système d'alignement](../files/gabarit_fixation.stl)

![Fixation](./images/fixation.jpg)

En approchant la tête du corps, les deux parties s'attirent grâce aux aimants et s'alignement grâce aux demi-sphères.

###### Le robot fini

Au final, après avoir apporté les différentes modifications au robot de base, nous avons un robot à tête amovible qui renferme une clé USB. Reste à l'imprimer...

[Fichier .stl pour imprimer le robot dans sa version finale](../files/robot.stl)

![Fixation](./images/robotimpr.jpg)

###### La présentation

Pour emporter et protéger le robot, nous avons utilisé une valise dans laquelle nous avons placé un carton découpé à la forme du robot pour pouvoir le garder en place. Tinkercad permet assez facilement d'obtenir la coupe d'un objet 3D. Il suffit de déplacer l'objet pour que le plan de construction corresponde au plan de coupe souhaité et d'exporter ensuite le fichier .svg. La découpe dans le carton correspond du coup exactement à la forme du robot.

[Fichier .svg pour la découpe de l'empreinte du robot](../files/empreinte_robot.svg)

![Valise](./images/empreinte.jpg)

###### Le message

Stéphanie a enregistré un message et a déformé le son de sa voix à l'aide de [Scratch](https://scratch.mit.edu). Dans ce message, le robot explique qu'il a été envoyé sur terre pour savoir à quoi ressemblera notre planète en 2500 et qu'il partagera ces informations lors d'un congrès intergalactique. Il dit ne comprendre que le langage numérique (.dxf, .gcode, .stl) et demande d'utiliser la clé USB pour communiquer avec lui.

[Message du robot](../files/message_robot.m4a)

Le robot est maintenant fin prêt pour partir à la rencontre des enfants.

#### L'immersion dans la fiction, la rencontre avec les enfants
Lors de la première séance, nous leur présentons la situation et nous leur faisons écouter le message du robot. Après décryptage, nous comprenons les besoins de l'extraterrestre. Nous devons imaginer la terre en 2500 afin de pouvoir lui transmettre des informations sur les objets du futur. Lors d'un brainstorming, nous rassemblons toutes leurs idées et nous les notons. Elles sont variées allant de la voiture sous-marine aux chevaux volants en passant par le sous-marin filtreur d'océan et les Pokémons vivants.

![Presentation](./images/yourte1.jpg)
![Presentation](./images/robot_yourte.jpg)
![Brainstorming](./images/brainstorming2.jpg)

Nous avons essayé de dégager 5 thèmes principaux :

* les robots;
* les transports volants;
* le monde sous-marin;
* la nature;
* l'imaginaire.

Nous avons réparti les enfants, en fonction de leur préférence, dans 5 groupes correspondant aux 5 thèmes. Sur une feuille de papier, nous leur demandons de dessiner, par groupe de deux ou trois, un objet du futur dans le thème qui leur est attribué. C'est ainsi que s'est clôturée la première séance dans la classe de Julie.

![dessin_filles](./images/strip_dessin_filles.jpg)

#### L'initiation au dessin 3D et à l'imprimante 3D
Les deuxième et troisième séances dans la yourte ont été consacrées à la 3D : conception en 3D, découverte du programme de dessin [Tinkercad]((https://www.tinkercad.com/search)) (libre et disponible en ligne) et impression 3D.


Chaque groupe d'enfants a jusqu'à présent imaginé et dessiné sur papier un objet du futur. Avant de passer au dessin en trois dimensions sur l'ordinateur, nous leur demandons de modéliser leurs objets en utilisant uniquement des solides géométriques (cube, parallélépipède rectangle, prisme, sphère, cône, etc.). Nous leur proposons de manipuler les solides afin de visualiser plus facilement les assemblages à réaliser pour amener leurs dessins dans la troisième dimension.

![solide](./images/strip_solide.jpg)


Nous les emmenons ensuite dans la salle informatique de l'école pour une initiation au programme Tinkercad. Après un rapide tutoriel leur expliquant les bases du programme, les enfants se mettent à la tâche. En une heure, ils sont tous parvenus à réaliser un dessin en trois dimensions de leur propre objet du futur.

![3D](./images/strip_3D.jpg)
![3D](./images/strip_3DD.jpg)

Enfin, ils découvrent l'impression 3D. Chacun de leur fichier sera transformé en .gcode puis imprimé au FabLab. Nous n'oublierons pas d'ensuite de remettre ses fichiers à l'extraterrestre, via la clé USB.

![3D](./images/3dprinter.jpg)


#### L'initiation au dessin 2D et aux découpeuses laser et vinyle
Les quatrième et cinquième séances dans la yourte consistent à faire découvrir les découpeuses laser et vinyle et à utiliser le logiciel libre [Inkscape](https://inkscape.org/fr/) pour réaliser des dessins en deux dimensions.

Afin de continuer à faire vivre l'histoire de notre extraterrestre, nous avons enregistré un nouveau message dans lequel il signale qu'il a bien reçu les fichiers .gcode qui lui sont parvenus via la clé USB, qu'il se demande ce que sont ces objets bizarres et dans quel environnement ils vivent. Il précise aussi que son départ pour l'espace est proche et que les enfants doivent se dépêcher de lui transmettre les informations nécessaires pour le congrès intergalactique.

Afin de répondre, une nouvelle fois, à la demande du robot extraterrestre, nous demandons aux enfants d'imaginer l'environnement des objets du futur qu'ils ont imaginés. Où vivront les chevaux volants par exemple ? Ou encore quel sera l'environnement des sous-marins filtrants ?

Chaque enfant imagine donc un élément de l'environnement futur et le dessine, d'abord sur papier puis en utilisant Inkscape. Ce design 2D est ensuite gravé sur du carton et découpé avec la laser ou bien dessiné puis découpé avec la  découpeuse vinyle. Ce carton prend ensuite place sur un plus grand dessin réalisé par les enfants.

![fresque](./images/strip_laser.jpg)


L'ensemble de tous les dessins forme une grande fresque du futur. Celle-ci est numérisée, avec un appareil photo, puis transmise au robot via la clé USB.

![fresque](./images/strip_fresque.jpg)


#### Visite du FabLab ULB et fin de la fiction

Pour terminer en grand notre fiction, nous proposons aux enfants une visite du FabLab ULB. Pendant une journée complète, ils sont accueillis au FabLab. Ils découvrent ainsi les machines qu'ils n'ont pas pu voir en classe telles que les fraiseuses numériques ou encore les grandes découpeuses laser. Ils visitent également le Frugal Lab, un laboratoire de recherche, et rencontrent les chercheurs, les techniciens et les fabmanageuses du FabLab ULB.

![fresque](./images/classe.jpg)
![fresque](./images/fraiseuse.jpg)
![fresque](./images/lab.jpg)

Cette visite est aussi l'occasion d'imprimer leur objet sur une des nombreuses imprimantes 3D et de regarder sa construction couche par couche.

La fresque du futur est enfin prête. En rassemblant leurs dessins, leurs découpes 2D, leurs impressions 3D, une image du futur qu'ils ont imaginé se dessine devant nous. Une photo est prise et déposée sur la clé USB du robot.  

![fresque](./images/fresque.jpg)

A la fin de la visite, nous les invitons à (re)déposer la valise noire à l'endroit où je l'avais découverte afin que l'extraterrestre puisse reprendre sa route...
