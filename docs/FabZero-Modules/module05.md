# 5. Electronique 1 - Prototypage

Cette semaine, nous avons eu une première expérience de prototypage à l'aide de cartes Arduino.

## Carte Arduino

Ce que j'ai compris... Une carte Arduino est une carte électronique qui se construit autour d'un microprocesseur, ici un Atmega328p. Ce microprocesseur peut effectuer différentes tâches : chaque patte peut servir d'entrée ou de sortie et le microprocesseur modifie l'état de ces pattes en fonction des ordres qui lui sont donnés. La force d'Arduino c'est d'avoir associé chaque patte à un nombre restreint d'actions possibles. Les pattes sont connectées à des pins que l'on peut utiliser pour réaliser certaines actions. Ainsi, l'utilisation de la carte est rendue plus facile pour des utilisateurs novices en électronique. Ça tombe bien...

Pour programmer une carte Arduino, il faut utiliser le logiciel Arduino et coder les instructions. Ensuite, il faut connecter la carte à son ordinateur grâce à un cable USB. Sur la carte, un microcontrôleur permet de convertir le signal envoyé par l'USB en un signal compréhensible par le miroprocesseur. Les instructions sont alors effectuées.

Attention, pour ne pas détruire une carte, il faut veiller à avoir :

* 5V max en entrée;
* 40 mA, voire 20mA, max en sortie par pin;
* 100 mA par groupe;
* 200 mA max pour l'ensemble de la carte.

## Premier exemple

Le premier défi a été de faire clignoter une LED. Avec Stéphanie, nous avons d'abord connecté la LED à la carte Arduino UNO en utilisant une breadboard. Puis nous avons connecté la carte à notre ordinateur et nous avons lancé le programme Blink dans les exemples présents dans le logiciel. On peut alors téléverser le programme et la LED se met à clignoter!

![Exemple](../images/arduino_exemple.jpg)
![Exemple](../images/arduino_blink.jpg)


<video controls muted>
<source src="../images/blink.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video>


## Deuxième exemple

Nous avons ensuite essayé de réaliser un feu de signalisation. Le montage est le suivant.

![Exemple](../images/traffic_light.jpg)

En allant chercher le code sur le site de [DFRobot](https://www.dfrobot.com/blog-595.html), nous avons pu faire fonctionner les feux et comprendre le code.

![Exemple](../images/traffic.jpg)

<video controls muted>
<source src="../../images/feu.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video>
