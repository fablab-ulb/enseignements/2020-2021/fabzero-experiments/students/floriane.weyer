# 4. Découpe assistée par ordinateur

Cette semaine nous avons appris à utiliser les découpeuses laser et vinyl.

## Découpeuse laser

Le principe de la découpeuse laser est assez simple : il suffit de fournir un fichier vectoriel de la découpe à réaliser et un laser va suivre ces chemins pour découper le matériau.

#### Calibration

Nous utilisons ici la découpeuse laser [Beamo](https://www.fluxlasers.com/products/beamo/) de chez Flux.

En fonction de la puissance et de la vitesse du laser, les découpes peuvent être plus ou moins profondes. Avant de débuter une découpe, il est utile de réaliser un test à plusieurs puissances et vitesses sur le matériau choisi. Pour ce faire, avec [Stéphanie](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module04/), nous avons créé un fichier sur Inkscape fait de plusieurs carrés de différentes couleur.

[Fichier .svg pour la calibration](../files/Calibration.svg)

![Calibration](../images/calibration.jpg)

Une fois le fichier prêt, on peut l'ouvrir dans le logiciel Beam Studio. Il faut ouvrir le fichier en mode "color" pour que le programme détecte les différentes couleurs et les associe à différents claques. Pour chaque couleur, on associe alors un couple puissance/vitesse. Ici, les valeurs sont exprimées en pourcentages de la puissance et de la vitesse max!

![Calibration](../images/beamo1.jpg)
![Calibration](../images/beamo2.jpg)

Pour voir le résultat de la découpe, il faut aller voir la [documentation de Stéphanie](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module04/).

#### Découpe d'un kirigami

Comme la découpeuse Beamo a eu quelques soucis, j'ai utilisé une découpeuse laser [Speedy 100](https://www.troteclaser.com/fr-be/machines-laser/machines-gravure-laser-speedy/) de chez Trotec. Grâce au site de [Nicolas de Coster](http://archive.fabacademy.org/2018/labs/fablabulb/students/nicolas-decoster/index.html?page=assignment&assignment=03) j'ai pu générer un fichier pour un kirigami dont j'ai choisi les paramètres. J'ai lancé la découpe avec les paramètres pré-enregistrés pour du carton de 1.5mm d'épaisseur. Le carton est fort épais et se déforme difficilement.

[Fichier .svg pour le kirigami](../files/Kirigami.svg)

![Pattern](../images/kirigami_pattern.jpg)

![Kirigami](../images/kirigami.jpg)

#### Découpe d'un origami

La découpeuse laser permet aussi de marquer des plis. Lorsque le laser ne traverse pas complètement le carton, des entailles se forment et permettent de plier la feuille. En m'inspirant de boules de noël en origami qu'on peut trouver sur internet, j'ai dessiné le développement de cette boule sur Inkscape.

[Fichier .svg pour l'origami](../files/Goutte.svg)

![Pattern](../images/origami_pattern.jpg)

![Origami](../images/origami.jpg)

## Découpeuse vinyl

Le principe de la découpeuse vinyl est fort similaire à celui de la découpeuse laser. Ici c'est une lame plutôt qu'un laser qui effectue la découpe.

Ici, j'avais envie de faire un sticker en forme de poule stylisée pour coller sur des t-shirts faits maison. Le sticker fait environ 2cmx2cm.

Il me reste à réaliser la découpe au FabLab. 

![Poule](../images/poule.jpg)
