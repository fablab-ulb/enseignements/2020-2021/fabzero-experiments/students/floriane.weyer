# 2. Conception Assistée par Ordinateur

Deux programmes de dessin 3D, _open source_, ont été présentés : OpenSCAD et FreeCAD. Les deux permettent de dessiner des objets en 3D mais leurs approches sont assez différentes. Le premier permet de "coder" les pièces, avec une approche très mathématique, alors que le second créent les pièces à l'aide des outils présents dans le logiciel.

## OpenSCAD

[Lien pour télécharger OpenSCAD.](https://www.openscad.org)

[Lien vers la _cheatsheet_.](https://www.openscad.org/cheatsheet/)


## FreeCAD

[Lien pour télécharger FreeCAD.](https://www.freecadweb.org/index.php)

## Création de FlexLinks

La BYU a développé un set complet de liens flexibles, compatibles avec les LEGO, appelés [FlexLinks](https://www.compliantmechanisms.byu.edu/flexlinks). En les utilisant dans des constuctions de LEGO, les structures peuvent devenir flexibles.

Le but est de reproduire certains de ces liens à l'aide des logiciels de dessin 3D. J'ai décidé d'utiliser le logiciel OpenSCAD pour m'essayer au codage de pièces 3D. En se basant sur la _cheatsheet_, on peut définir des formes géométriques et les paramétrées assez facilement.

#### Première pièce : Fixed-Fixed Beam

[Fichier .stl](../files/Fixed_Fixed.stl)

La pièce est constituée de deux têtes reliées par une tige centrale. La pièce est paramétrique : on peut changer le nombre de trous dans les têtes, la distance entre ces trous, l'épaisseur d'un bord, la longueur de la tige, la hauteur de la pièce. Pour modifier les propriétés mécaniques de la pièce la longueur et l'épaisseur de la tige peuvent facilement être modifiées. À partir de cette pièce, on peut facilement obtenir la Cantilever Beam : il suffit d'enlever la seconde tête.

```
/*
Fichier : Fixed_Fixed.scad

Auteur : Floriane Weyer

Date : 02/03/21

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/

$fn = 50;

// Paramètres

// Hauteur de la pièce
height = 6.4;

// Nombre de trous
n = 2;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 0.7;

//Longueur de la tige
beam_length = 40;
// Epaisseur de la tige
beam_thickness = 1.5;

//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = True);
                }
            }
        }
    }
// Création de la première tête
head();

// Création de la deuxième tête
translate([-((n-1)*distance)-(2*radius)-(2*edge_thickness)-beam_length,0,0]){
    head();
    }    

// Création de la tige entre les deux têtes   
translate([-beam_length-radius-edge_thickness, -beam_thickness/2,0]){
    cube([beam_length,beam_thickness,height], center = True);
    }
```

Voici deux exemples obtenus en utilisant le même code mais en changeant le nombre de trous et la longueur de la tige.
![Exemples](../images/piece_2trous.jpg)
![Exemples](../images/piece_1tete.jpg)

#### Deuxième pièce : Cross-Axis Flexural Pivot

[Fichier .stl](../files/Cross_Axis.stl)

La pièce est constituée de deux têtes parallèles reliées par deux tiges croisées. On peut éventuellement retirer la deuxième tige et obtenir une autre pièce.

```
/*
Fichier : Cross_Axis.scad

Auteur : Floriane Weyer

Date : 02/03/21

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/
$fn=50;
// Paramètres

// Hauteur de la pièce
height = 6.4;

// Nombre de trous
n = 4;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 0.7;

//Distance selon y entre les deux têtes
head_distance = 40;

// Epaisseur de la tige
beam_thickness = 1.5;

// Calcul de la longueur de la tige et de l'angle
beam_length = sqrt(pow(head_distance,2)+pow((n-1)*distance,2))-(2*radius);
angle = atan(head_distance/((n-1)*distance));


//Définition du module z
module z(){
//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height/2, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height/2, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height/2, r = radius, center = True);
                }
            }
        }
    }
// Création de la première tête
head();

// Création de la deuxième tête
translate([0,head_distance,0]){
    head();
    }    

// Création de la première tige entre les deux têtes   
rotate([0,0,angle])   
translate([radius,-beam_thickness/2,0])
cube([beam_length,beam_thickness,height/2], center = True);
}

z();

translate([0,head_distance,0])
rotate([180,0,0])
z();
```

![Exemples](../images/piece_croix32.jpg)
![Exemples](../images/piece_diag.jpg)

#### Troisième pièce : Half-Circle

[Fichier .stl](../files/Half_Circle.stl)

La pièce est constituée de deux têtes parallèles reliées par une tige en demi-cercle.

```
/*
Fichier : Half_Circle.scad

Auteur : Floriane Weyer

Date : 02/03/21

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/

$fn=50;
// Paramètres

// Hauteur de la pièce
height = 6.4;

// Nombre de trous
n = 2;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 0.7;

//Rayon de courbure interne
beam_radius = 30;
// Epaisseur de la tige
beam_thickness = 1.5;

//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = True);
                }
            }
        }
    }
// Création de la première tête
head();

// Création de la deuxième tête
translate([0, (2*beam_radius)+(beam_thickness),0])
head();

// Création de la tige entre les deux têtes   
translate([-radius-edge_thickness, beam_radius+(beam_thickness/2),0])
difference(){
    cylinder(h = height, r = beam_radius + beam_thickness, center = True);
    cylinder(h = height, r = beam_radius, center = True);
    translate([0,-(2*(beam_radius+beam_thickness)),0])
    cube([(2*(beam_radius+beam_thickness)), (4*(beam_radius+beam_thickness)), 2*height]);
    }
```

![Exemples](../images/piece_cercle.jpg)

#### Quatrième pièce : Out of Plane

[Fichier .stl](../files/Out_of_Plane.stl)

Cette pièce est la même que la précédente mais les têtes sont tournées de 90°.

```
/*
Fichier : Out_of_Plane.scad

Auteur : Floriane Weyer

Date : 02/03/21

Licence : Creative Commons Attribution 4.0 International (CC BY 4.0)

//*/

$fn=50;
// Paramètres

// Hauteur de la tête
height = 6.4;

// Nombre de trous
n = 2;
// Rayon interne des trous
radius = 2.5;
// Distance centre à centre entre deux trous
distance = 8;
// Distance entre l'extérieur du trou et l'extérieur de la pièce
edge_thickness = 0.7;

//Rayon de courbure interne
beam_radius = 30;
// Epaisseur de la tige
beam_thickness = 1.5;

//Définition du module head
module head(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+edge_thickness, center = True);
            translate([(n-1)*distance,0,0]){cylinder(h = height, r = radius+edge_thickness, center = True);}
            }
        for (i = [1:n]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = True);
                }
            }
        }
    }
// Création de la première tête
translate([0,(height/2),radius+edge_thickness])
rotate([90,0,0])    
head();

// Création de la deuxième tête    
translate([0, (height/2)+(2*beam_radius)+(beam_thickness),radius+edge_thickness])
rotate([90,0,0])
head();

// Création de la tige entre les deux têtes   
translate([-radius, beam_radius+(beam_thickness/2),0])
difference(){
    cylinder(h = 2*(radius+edge_thickness), r = beam_radius + beam_thickness, center = True);
    cylinder(h = 2*(radius+edge_thickness), r = beam_radius, center = True);
    translate([0,-(2*(beam_radius+beam_thickness)),0])
    cube([(2*(beam_radius+beam_thickness)), (4*(beam_radius+beam_thickness)), 2*(radius+edge_thickness)]);
    }
```

![Exemples](../images/piece_outofplane.jpg)
