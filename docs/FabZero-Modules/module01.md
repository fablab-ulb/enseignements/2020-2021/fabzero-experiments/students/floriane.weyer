# 1. Documentation

## Installation de Git

Git est un logiciel de contôle de versions, il permet de créer une copie d'une page web sur mon ordinateur pour pouvoir y travailler puis remettre cette copie sur GitLab.

* Installation de Xcode :
Installé en tapant ```xcode-select --install ``` dans le terminal.
* Installation de Homebrew :
Installé en allant sur le site [Homebrew](https://brew.sh/index.html) et en tapant ```/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"```   dans le terminal.
* Installation de Git  :
Installé en tapant ```brew install git```dans le terminal.

## Configuration de Git
Confguration de Git en tapant dans le terminal

* ```git config --global user.name "your_username"```
* ```git config --global user.email "your_email_address@example.com" ```

avec mon nom d'utilisateur et mon adresse mail.

## Méthodes d'authentification de Git
Deux méthodes peuvent être utilisées pour s'authentifier : HTTPS ou SSH. J'ai choisi la deuxième car elle évite de devoir donner son nom d'utilisateur et son mot de passe à chaque fois

* Génération d'un clé SSH :
J'ai tapé dans le terminal ```ssh-keygen -t ed25519 -C "<comment>"``` en mettant en commentaire mon adresse mail. Le terminal m'a répondu ```Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/user/.ssh/id_ed25519):```. J'ai accepté. J'ai encodé une phrase d'authentification à deux reprises et le terminal m'a alors confirmé que la clé avait été créée.
* Ajout de la clé à mon compte GiltLab :
J'ai copié la clé grâce à ```pbcopy < ~/.ssh/id_ed25519.pub```. Je me suis rendue sur le site de GitLab, j'ai édité mon profil. En allant dans l'onglet SSH Keys, j'ai pu coller ma clé dans le cadre Key, indiqué une description dans le cadre Title et terminé en cliquant que le bouton Add.
* Vérification :
Pour s'assurer que la clé a bien été ajoutée, j'ai tapé ```ssh -T git@gitlab.com``` dans le terminal. Le terminal m'a alors demandé si je voulais continuer la connection, j'ai tapé ```yes``` et GitLab m'a souhaité la bienvenue!

## Clonage d'un _repository_
Pour travailler sur mon site web localement à partir du _repository_ présent sur GitLab, je dois le cloner. Dans FabZero_experiments > students > floriane.weyer > Repository, je clique sur le bouton _Clone_ le _repository_ et je choisis SSH.  A partir du dossier où je souhaite travailler, j'ouvre alors un terminal et je tape ```git clone <repository path>``` en remplaçant le <repository path> par le _path_ copié en clonant. Les fichiers présents sur GitLab sont maintenant sur ma machine et je peux y travailler.

## De GitLab vers la copie locale
Pour obtenir les derniers changements faits sur un projet, je tape ```git pull origin master ``` dans le terminal ouvert dans le dossier où j'ai cloné le _repository_.

## Valider les modifications
Pour valider les modifications apportées sur ma copie locale, je tape ```git add . ``` puis ```git commit -m "avec une description de l'action"```.

## De la copie locale vers GitLab
Pour envoyer les modifications faites sur ma copie locale à GitLab, je tape ```git push origin master ```dans le terminal ouvert dans le dossier où j'ai cloné le _repository_.

![Résumé des commandes](../images/resume.jpg)
