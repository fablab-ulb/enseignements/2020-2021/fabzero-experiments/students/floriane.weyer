# 6. Electronique 2 - Fabrication

Cette semaine, nous avons appris à faire notre propre Arduino.

## Mise en place et soudure de tous les éléments

Grâce à la [documentation](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/electronics/-/blob/master/Make-Your-Own-Arduino.md) fournie par Nicolas de Coster, nous avons pu souder les différents éléments de la carte. En suivant les indications et avec un peu (beaucoup) de patience, il est possible de réaliser son propre Arduino. Je renvoie à la documentation de Nicolas pour les instructions et je mets en photos les différentes étapes de réalisation.

#### Première étape

Mise en place du microprocesseur, du régulateur, du condensateur et d'une résistance.

![Carte première étape](../../images/carte_1.jpg)

#### Verification et premier test

Après la première étape, Nicolas a vérifié que la carte était fonctionnelle. J'ai pu alors configurer le software Arduino pour interagir avec la carte.

![Conf1](../images/conf_1.jpg)
![Conf2](../images/conf_2.jpg)
![Conf3](../images/conf_3.jpg)
![Conf4](../images/conf_4.jpg)
![Conf5](../images/conf_5.jpg)
![Conf6](../images/conf_6.jpg)
![Conf7](../images/conf_7.jpg)
![Conf8](../images/conf_8.jpg)
![Conf9](../images/conf_9.jpg)

J'ai utilisé le [programme Blink](../files/Blink_moi.ino) proposé par Nicolas pour tester la LED.

<video controls muted>
<source src="../../images/blinking.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video>

#### Deuxième étape

Mise en place d'une LED, du bouton, de résistance et des pins.

![Carte deuxième étape](../images/carte_2.jpg)

#### Verification et deuxième test

En utilisant le [programme](../files/Interrupteur.ino) proposé par Nicolas pour tester l'interrupteur, j'ai pu vérifier le bon fonctionnement de ma carte.

<video controls muted>
<source src="../../images/button.mp4" type="video/mp4">
Cette vidéo ne semble pas fonctionner dans votre browser.
</video>
