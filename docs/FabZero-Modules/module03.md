# 3. Impression 3D

Cette semaine, nous avons appris à utiliser les imprimantes 3D Prusa pour réaliser notre kit de FlexLinks.

## Licence
J'ai ajouté un en-tête à mes codes pour préciser le nom du fichier, l'auteur, la date et la licence choisie (Creative Commons Attribution 4.0 International (CC BY 4.0)). Il est aussi possible de citer les auteurs des codes sur lesquels je me base ou d'autres contributeurs.

## Dimensions des pièces

La semaine passée, j'ai créé quatre FlexLinks différents. Avant de les imprimer, j'ai dû modifier leurs dimensions pour les rendre compatibles avec les LEGO.

[Stéphanie](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/) a mesuré les pièces, les trous, les espacements. Elle a également réalisé un calibrateur de trous, malin! J'ai pu me baser sur son [travail](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/stephanie.krins/FabZero-Modules/module02/) pour redimensionner mes pièces. Merci!

J'ai mis à jour mes codes et mis en ligne les fichiers .stl obtenus.

Les pièces sont prêtes à être imprimées.

* [Fixed_Fixed.stl](../files/Fixed_Fixed.stl)
* [Cross_Axis.stl](../files/Cross_Axis.stl)
* [Half_Circle.stl](../files/Half_Circle.stl)
* [Out_of_Plane.stl](../files/Out_of_Plane.stl)

## Pièces supplémentaires

Pour ajouter des pièces supplémentaires à mon kit, j'ai utilisé les fichiers partagés par d'autres étudiants. J'ai choisi la pièce en arc de cercle de [Paul](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/) ([lien vers le code de la pièce](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-experiments/students/paul.bryssinck/-/tree/master/docs/CAD%20Files)) et la pièce avion de [Doriane](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/) ([lien vers le code de la pièce](https://fablab-ulb.gitlab.io/enseignements/2020-2021/fabzero-experiments/students/doriane.galbez/FabZero-Modules/module02/)).

Après avoir redimensionné et modifié légèrement leurs codes, j'ai deux nouvelles pièces pour mon kit.

* [Curved_Beam.stl](../files/Curved_Beam.stl)
* [Airplane.stl](../files/Airplane.stl)

## Impression des pièces

Les imprimantes 3D disponibles au FabLab sont des Prusa. Pour pouvoir imprimer les pièces à partir des fichiers .stl, il faut installer le [PrusaSlicer](https://www.prusa3d.fr/prusaslicer/) puis ouvrir les fichiers à imprimer. On peut alors les déposer de la manière la plus adéquate sur la plaque d'impression.

![Exemples](../images/impression_1.jpg)

Il faut ensuite régler les paramètres d'impressions : la hauteur de couche (moitié du diamètre de la buse, 0.2 mm ici car buse de 0.4 mm), le nombre de parois verticales et horizontales, la densité du remplissage, le motif de remplissage, les jupes et bordures, les supports.

![Exemples](../images/impression_2.jpg)
![Exemples](../images/impression_3.jpg)

Il faut régler les paramètres du filament en fonction de quel filament on utilise sur l'imprimante. Pour l'impression de mon kit, ce sera du PETG même si, au FabLab c'est principalement du PLA qui est utilisé.

![Exemples](../images/impression_4.jpg)

Enfin, on choisit l'imprimante utilisée. Au FabLab, il y a des Prusa i3 MK3 et MK3S avec buses de 0.4 ou 0.6mm. Mais pour la réalisation de mon kit, Stéphanie a accepté d'imprimer mon fichier sur sa Prusa Mini.

![Exemples](../images/impression_5.jpg)

Une fois les paramètres réglés, il suffit de cliquer sur "Imprimer maintenant" puis sur "Générer le G-code" pour obtenir un fichier à charger sur une carte SD et à insérer dans l'imprimante. Reste à lancer l'impression...

## Machine

Difficile pour moi d'imaginer une machine originale faite de LEGO et de FlexLinks. J'espère avoir plus d'inspiration en ayant les pièces en mains et en essayant. La vidéo viendra plus tard...
